// mongoose -> https://mongoosejs.com/docs/guide.html#definition
import mongoose from 'mongoose';

// create a schema
const User = new mongoose.Schema({
	email: {
		type: String,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	salt: String,
	allowNewsletters: {
		type: Boolean,
		default: true
	},
	termsAccepted: {
		type: Boolean,
		default: false
	},
	status: String,
	created_at: {
		type: Date,
		default: new Date()
	},
	updated_at: {
		type: Date,
		default: new Date()
	}
});

export default mongoose.model('User', User);

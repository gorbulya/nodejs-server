/**
 * recomendation:
 * 1. http://expressjs.com/ (nodejs)
 * 2. https://mongoosejs.com/docs/ ()
 **/

import '@babel/polyfill';
import "regenerator-runtime/runtime.js";

import express from 'express';
import appLoader from './loaders';
import config from './config';

async function startServer() {
	const app = express();

	await appLoader({app, config});

	app.listen(config.PORT, err => {
		if (err) {
			process.exit(1);
			return;
		}

		console.log(`
        ################################################
        Server listening on port: ${config.PORT} and version: ${config.SERVICE_PREFIX}/v${config.VERSION}
        🛡️################################################`);
	});
}

startServer();

export default startServer;


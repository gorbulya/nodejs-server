import mongoose from 'mongoose';

/**
 * @desc З'єднання з Базою Даних
 * @return {Promise}
 **/
export async function connectToMongoDb({config}) {
	const connection = await mongoose
		.connect(
			config.MONGODB_URI, {
				useNewUrlParser: true,
				useCreateIndex: true,
				useUnifiedTopology: true
			});

	return connection.connection.db;
}
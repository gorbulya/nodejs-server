import expressLoader from './express';
import {connectToMongoDb as mongoDBLoader} from './mongodb';

export default async ({app, config}) => {
	await mongoDBLoader({config});
	console.log('✌ MongoDB is connected.');

	await expressLoader({app, config});
	console.log('✌ Express loaded.');
}
import {UserModel} from "../db/models";

export default class UserService {
	/**
	 * @desc Метод для створення користувача в БД
	 **/
	static async createUser(userODI) {
		const {
			name,
			email,
			password
		} = userODI;

		const user = await UserModel.create({
			name,
			email,
			password
		});

		return user;
	}
}
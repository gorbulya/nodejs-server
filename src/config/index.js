import dotenv from 'dotenv';

const environment = dotenv.config();

if (!environment) {
	throw new Error('Config file was not found.');
}

export default {
	/**
	 * @desc Порт на якому працює пріложуха
	 * @type {Number}
	 **/
	PORT: process.env.PORT || 3005,

	/**
	 * @desc Префікс API
	 **/
	SERVICE_PREFIX: '/api',//process.env.SERVICE_PREFIX || '/api',

	/**
	 * @desc Версія софта
	 * @type {String}
	 **/
	VERSION: process.env.VERSION || '1.0.0',

	/**
	 * @desc Параметр для зв'язку з БД
	 **/
	MONGODB_URI: process.env.MONGODB_URI || '',

	/**
	 *
	 **/
	MAILGUN_DOMAIN: process.env.MAILGUN_DOMAIN || null,

	/**
	 *
	 **/
	MAILGUN_API_KEY: process.env.MAILGUN_API_KEY || null
}
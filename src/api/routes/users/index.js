import {Router} from 'express';
import {createUserController} from './controller';

// help information
// https://expressjs.com/ru/4x/api.html#router
const route = Router();

export default mainRoute => {
	mainRoute.use('/users', route);

	// POST -> http://<YOUR-DOMAIN-NAME>/api/v2/users
	route.post('/', createUserController);


	// GET -> http://<YOUR-DOMAIN-NAME>/users
	/**
	 * @desc Router для виводу списка користувачів -> http://.../users
	 **/
	route.get(
		'/',
		(request, response, next) => {

			response.json({
				success: true,
				data: []
			});
		});


	// PUT -> http://<YOUR-DOMAIN-NAME>/users/123
	route.put('/:id', (request, response, next) => {

	});
}
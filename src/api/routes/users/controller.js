import UserService from "../../../services/userService";

/**
 * @desc Логіка по обробці створення користувача
 **/
export const createUserController = async (request, response, next) => {
	const {name, email, password} = request.body;

	const user = await UserService.createUser({
		name, email, password
	});

	return response.json({
		success: true,
		data: user
	});
}
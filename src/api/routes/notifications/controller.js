import MailService from "../../../services/mailService";

/**
 *
 **/
export const sendEmailController = async (request, response) => {
	const {to, message} = request.body;

	await MailService.send(to, message);

	response.json({
		success: true,
		data: 'just for test'
	});
}
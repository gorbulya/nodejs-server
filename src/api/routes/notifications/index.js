import {Router} from 'express';
import {sendEmailController} from "./controller";

// help information
const route = Router();

export default mainRoute => {
	mainRoute.use('/notifications', route);

	route.post('/send-email', sendEmailController);
}
import {Router} from 'express';

import users from './routes/users';
import notifications from './routes/notifications'

export default () => {
	const router = Router();

	users(router);
	notifications(router);

	return router;
}
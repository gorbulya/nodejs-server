import {UserModel} from "../db/models";
import jwt from 'jsonwebtoken';
import config from '../config';
import {randomBytes} from 'crypto';
import argon2 from 'argon2';
import {GeneralError, AccessDenied} from "../errors";


export default class UserService {

	/**
	 * @desc Авторизація користувача
	 **/
	static async signin({email, password}) {
		const user = await UserModel.findOne({
			email
		});

		if (!user) {
			throw new GeneralError('User not found');
		}

		const verification = await argon2.verify(
			user.password,
			password,
			{
				salt: user.salt
			}
		);

		if (!verification) {
			throw new AccessDenied('Invalid Password');
		}

		const result = user.toObject();

		delete result.__v;
		delete result.salt;
		delete result.password;

		return {
			...result,
			token: UserService.generateToken(user)
		};
	}

	static generateToken(user) {
		const today = new Date();
		const exp = new Date(today);
		exp.setDate(today.getDate() + 60);

		return jwt.sign(
			{
				_id: user._id,
				name: user.name,
				exp: exp.getTime() / 1000,
			},
			config.JWT_SECRET,
		);
	}

	/**
	 * @desc Створення користувача та генерація токена
	 **/
	static async signUp({password, email, name}) {
		const salt = randomBytes(32);
		const pswd = await argon2.hash(password, {salt});
		const user = await UserService.createUser({
			email,
			name,
			salt,
			password: pswd
		});



		const result = user.toObject();

		delete result.__v;
		delete result.salt;
		delete result.password;

		return {
			...result,
			token: UserService.generateToken(user)
		}
	}

	/**
	 * @desc Вибірка Користувача по ідентифікатору
	 * @return {Promise<Object>}
	 **/
	static async getUserById(id) {
		return UserModel.findById(id);
	}

	/**
	 * @desc Видалення користувача
	 **/
	static async deleteUserById(id) {
		return UserModel.findByIdAndDelete(id);
	}

	/**
	 * @desc Вибірка користувачів з БД
	 * @return {Promise<Object>}
	 **/
	static async getUsers() {
		return UserModel
			.find({})
		// .select('-password');
	}

	/**
	 * @desc Метод для створення користувача в БД
	 **/
	static async createUser(userODI) {
		const {
			name,
			email,
			salt,
			password
		} = userODI;

		const user = await UserModel.create({
			name,
			email,
			salt,
			password
		});

		return user;
	}
}
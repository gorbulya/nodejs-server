import MailService from "./mailService";
import StatusService from "./statusService";
import UserService from "./userService";

export {
	MailService,
	UserService,
	StatusService
};
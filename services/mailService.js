import mailgun from 'mailgun-js';
import config from '../config';

const mailgunProvider = mailgun({
	apiKey: config.MAILGUN_API_KEY,
	domain: config.MAILGUN_DOMAIN
});

export default class MailService {

	/**
	 *
	 **/
	static async send(to, text) {
		const data = {
			to,
			text,
			subject: 'Testing the MailGun Service',
			from: 'Excited User <me@samples.mailgun.org>'
		};

		return new Promise((resolve, reject) => {
			mailgunProvider
				.messages()
				.send(data, function (error, body) {
					if (error) {
						return reject(error);
					}

					return resolve({
						message:'Message has been send!!!!',
						body
					});
				});
		})
	}
}
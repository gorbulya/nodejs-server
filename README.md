# fe-16-nodejs-server

# Встановлення проекту

Перше з чого потрібно почати запуск системи - це створити файл `.env` та по прикладу `.env.example` заповнити його змінними.

# Запуск системи

Для того щоб запустити систему в режимі розробки - необхідно виконати наступну команду

`# npm run start:dev`


# Тести

Читаємо [тут](https://bcostabatista.medium.com/testing-nodejs-applications-with-jest-7ae334daaf55)!


# Шаблонізатор

Чиатємо [тут](https://pugjs.org/api/getting-started.html)
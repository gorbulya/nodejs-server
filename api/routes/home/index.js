import {Router} from 'express';
import {
	homeController,
	aboutController,
	productsController
} from './controller';

// help information
// https://expressjs.com/ru/4x/api.html#router
const route = Router();

export default mainRoute => {
	mainRoute.use('/', route);

	// POST -> http://<YOUR-DOMAIN-NAME>/api/v2/
	route.get('/', homeController);

	route.get('/about', aboutController);

	route.get('/products', productsController);
}

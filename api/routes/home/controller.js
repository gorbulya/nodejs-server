const menu = [
	{label: 'Home', url: '/v2/'},
	{label: 'Products', url: '/v2/products'},
	{label: 'About', url: '/v2/about'},
];

export const homeController = (request, response) => {
	response.render('index', {
		friends: 10,
		user: {
			name: 'Sergey',
			saldo: 1234.56
		},
		menu
	});
}

export const aboutController = (request, response) => {
	response.render('about', {menu});
}

export const productsController = (request, response) => {
	response.send('Test string for product!!!!');
}
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmNmZDM2ZTNmMWNjYThmMWI5MTQ0MDUiLCJuYW1lIjoiQXJ0ZW0iLCJleHAiOjE2MTI2Mzk1OTguNzkxLCJpYXQiOjE2MDc0NTU1OTh9.EeP6TPMGjTghWSBCO6q7fyTzYfp7eV6glazLwIXpJxA

import {
	UserService,
	StatusService
} from "../../../services";

import {GeneralError} from "../../../errors";

/**
 * @desc Логіка по обробці створення користувача
 **/
export const createUserController = async (request, response, next) => {
	const {name, email, password} = request.body;

	const user = await UserService.createUser({
		name, email, password
	});

	return response
		.json(
			StatusService
				.buildResponse(true, user)
		);
}

/**
 * @desc Логіка по вибірці списку користувачів з бази даних
 **/
export const getUsersController = async (request, response, next) => {

	const users = await UserService.getUsers();

	response.json(
		StatusService.buildResponse(true, users)
	);
}

/**
 * @desc Видалення користувача по ідентифікатору
 **/
export const deleteUserController = async (request, response, next) => {
	try {
		const {userId} = request.params;
		const deletedObject = await UserService.getUserById(userId);

		if (!deletedObject) {
			throw new GeneralError('No such user');
		}

		await UserService.deleteUserById(userId);

		response
			.status(200)
			.json(
				StatusService.buildResponse(true, deletedObject)
			);
	} catch (e) {
		response
			.status(e.status)
			.json(
				StatusService.buildError(e.message)
			);
	}
}
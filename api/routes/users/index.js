import {Router} from 'express';
import {
	getUsersController,
	createUserController,
	deleteUserController
} from './controller';
import isAuth from "../../middleware/isAuth";

// help information
// https://expressjs.com/ru/4x/api.html#router
const route = Router();

export default mainRoute => {
	mainRoute.use('/users', route);

	// POST -> http://<YOUR-DOMAIN-NAME>/api/v2/users
	route.post('/', createUserController);


	// GET -> http://<YOUR-DOMAIN-NAME>/users
	/**
	 * @desc Router для виводу списка користувачів -> http://.../users
	 **/
	route.get('/',
		isAuth,
		getUsersController
	);

	// DELETE -> http://<YOUR-DOMAIN-NAME>/users/:userId
	/**
	 * @desc Видалення користувача
	 **/
	route.delete('/:userId', deleteUserController);


	// PUT -> http://<YOUR-DOMAIN-NAME>/users/123
	route.put('/:id', (request, response, next) => {

	});
}
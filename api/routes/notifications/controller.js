import {MailService, StatusService} from "../../../services";

/**
 *
 **/
export const sendEmailController = async (request, response) => {
	const {to, message} = request.body;

	await MailService.send(to, message);

	response.json(
		StatusService
			.buildResponse(true, 'just for test')
	);
}
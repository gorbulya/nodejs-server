/**
 * @desc Реєстрація користувача та генерація Веб-токену
 **/
import {StatusService, UserService} from "../../../services";

export const signUpUserController = async (request, response) => {
	try {
		response.json(
			StatusService.buildResponse(
				true,
				await UserService.signUp(request.body)
			)
		);
	} catch (e) {
		response.json(StatusService.buildError(e.message, e.status));
	}
}

export const signInUserController = async (request, response) => {
	try {
		response.json(
			StatusService.buildResponse(
				true,
				await UserService.signin(request.body)
			)
		);
	} catch (e) {
		response.json(StatusService.buildError(e.message, e.status));
	}
}
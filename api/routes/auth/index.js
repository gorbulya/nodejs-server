import {Router} from 'express';
import {
	signInUserController,
	signUpUserController
} from './controller';

// help information
// https://expressjs.com/ru/4x/api.html#router
const route = Router();

export default mainRoute => {
	mainRoute.use('/auth', route);

	// POST -> http://<YOUR-DOMAIN-NAME>/api/v2/auth/signup
	route.post('/signup', signUpUserController);

	// POST -> http://<YOUR-DOMAIN-NAME>/api/v2/auth/signin
	route.post('/signin', signInUserController);
}
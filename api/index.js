import {Router} from 'express';

import auth from './routes/auth';
import home from './routes/home';
import users from './routes/users';
import notifications from './routes/notifications'

export default () => {
	const router = Router();

	home(router);
	auth(router);
	users(router);
	notifications(router);

	return router;
}
import BaseError from "./BaseError";

export default class AccessDenied extends BaseError {
	constructor(message) {
		super(message, 401);
	}
}
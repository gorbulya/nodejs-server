import BaseError from "./BaseError";

export default class GeneralError extends BaseError {
	constructor(message) {
		super(message, 500);
	}
}
import GeneralError from "./GeneralError";
import AccessDenied from "./AccessDenied";

export {
	AccessDenied,
	GeneralError
};